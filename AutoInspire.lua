local update_original = PlayerStandard.update
local init_original = PlayerStandard.init

PlayerStandard.AUTO_SHOUT_COOLDOWN = PlayerStandard.AUTO_SHOUT_COOLDOWN or 3
PlayerStandard._AUTO_INSPIRE_RECHECK_DELAY = 0.5

function PlayerStandard:init(...)
	self._last_inspire = {}
	self._last_shout_t = 0
	self._intimidate_t = 0
	self._auto_inspire_recheck_t = 0
	self._auto_inspire_cooldown = tweak_data.upgrades.morale_boost_base_cooldown * managers.player:upgrade_value("player", "morale_boost_cooldown_multiplier", 1)
	
	return init_original(self, ...)
end

function PlayerStandard:update(t, dt, ...)
	update_original(self, t, dt, ...)
	
	if t > self._auto_inspire_recheck_t then
		local action_forbidden = not managers.player:has_category_upgrade("player", "morale_boost") or
			self:chk_action_forbidden("interact") or 
			self._unit:base():stats_screen_visible() or 
			self:_interacting() or 
			self._ext_movement:has_carry_restriction() or 
			self:is_deploying() or 
			self:_changing_weapon() or 
			self:_is_throwing_grenade() or 
			self:_is_meleeing() or 
			self:_on_zipline() or
			managers.groupai:state():whisper_mode() or
			self._unit:character_damage():is_downed()
			
		
		if not action_forbidden then
			self:_update_autoinspire(t, dt)
		end
	end
end

function PlayerStandard:_update_autoinspire(t, dt)
	local rally_data = self._ext_movement:rally_skill_data()
	
	if rally_data.morale_boost_delay_t and t <= rally_data.morale_boost_delay_t then
		self._auto_inspire_recheck_t = t + PlayerStandard._AUTO_INSPIRE_RECHECK_DELAY
		return
	end
	
	local potential_targets = {}
	
	for u_key, u_data in pairs(managers.groupai:state():all_char_criminals()) do
		if alive(u_data.unit) and managers.player:player_unit():key() ~= u_key then
			local unit = u_data.unit
			local last_inspire_t = self._last_inspire[u_key] or 0
			local dist_sq = mvector3.distance_sq(self._pos, u_data.m_pos)
			
			if unit:in_slot(3) and not unit:movement():downed() and dist_sq <= rally_data.range_sq then
				local vec = unit:movement():m_head_pos() - self._ext_movement:m_head_pos()--self._ext_camera:position()
				local angle = vec:angle(self._ext_camera:forward())
				local distance = mvector3.normalize(vec)
				local max_angle = math.max(8, math.lerp(30, 10, distance / 1200))
				
				if angle <= max_angle then
					local duration_remaining = math.max(tweak_data.upgrades.morale_boost_time - (t - last_inspire_t), 0)
					local duration_threshold = tweak_data.upgrades.morale_boost_time - self._auto_inspire_cooldown
					
					if duration_remaining <= duration_threshold then
						local weight = 1
						--local peer = managers.network:game():member_from_unit(unit):peer()
						--local peer_id = peer:id()
						local peer_id = managers.criminals:character_peer_id_by_unit(unit)
						local peer = managers.network:session():peer(peer_id)
						local armor_level = tweak_data.blackmarket.armors[peer:blackmarket_outfit().armor].upgrade_level
						local armor_speed_penalty = tweak_data.upgrades.values.player.body_armor.movement[armor_level]
						local carry = managers.player:get_synced_carry(peer_id)
						
						local armor_mod = 1 / math.sqrt(armor_speed_penalty)
						local duration_mod = (1 - 0.5 * duration_remaining / duration_threshold)
					
						weight = weight * armor_mod
						weight = weight * duration_mod
						
						local carry_mod = 1
						if carry then
							local carry_tweak = tweak_data.carry[carry.carry_id]
							local carry_type_tweak = tweak_data.carry.types[carry_tweak.type]
							carry_mod = 1 / ((carry_type_tweak.move_speed_modifier or 1) * (carry_type_tweak.can_run and 1 or 0.9))
							
							weight = weight * carry_mod
						end

						table.insert(potential_targets, { unit = unit, key = u_key, id = peer_id, weight = weight, carry_mod = carry_mod, duration_mod = duration_mod, name = peer:name() })
					end
				end
			end
		end
	end
	
	if #potential_targets > 0 then
		local target
		for i, candidate in ipairs(potential_targets) do
			if not target or target.weight < candidate.weight then
				target = candidate
			end
		end
		
		self._last_inspire[target.key] = t
		rally_data.morale_boost_delay_t = t + self._auto_inspire_cooldown
		self._auto_inspire_recheck_t = t + self._auto_inspire_cooldown
		self:_on_auto_inspire(t, target)
		target.unit:network():send_to_unit( { "long_dis_interaction", target.unit, 1, self._unit } )
		
		--Buff list support
		if managers.gameinfo then
			managers.gameinfo:event("buff", "activate", "inspire_debuff")
			managers.gameinfo:event("buff", "set_duration", "inspire_debuff", { duration = self._auto_inspire_cooldown })
		end
	else
		self._auto_inspire_recheck_t = t + PlayerStandard._AUTO_INSPIRE_RECHECK_DELAY
	end
end

function PlayerStandard:_on_auto_inspire(t, data)
	local last_t = math.max(self._last_shout_t, self._intimidate_t)
	if last_t + PlayerStandard.AUTO_SHOUT_COOLDOWN < t then
		local shout_chance = math.min(0.45 * data.duration_mod * math.sqrt(data.carry_mod), 0.75)
		local roll = math.random()
		
		if shout_chance > roll then
			local gesture = self._ext_movement:current_state_name() ~= "mask_off" and "cmd_gogo"
			self:_do_action_intimidate(t, gesture, "g18", false)
			self._last_shout_t = t
			self._intimidate_t = t - tweak_data.player.movement_state.interaction_delay * 0.5
			return true
		end
	end
end