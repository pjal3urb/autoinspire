{
	"name": "Auto-Inspire",
	"description": "Automatically uses the inspire basic skill on non-AI team mates. Standard limitations apply, with simple target prioritization",
	"blt_version": 2,
		"hooks": [
		{ "hook_id": "lib/units/beings/player/states/playerstandard", "script_path": "AutoInspire.lua" }
	]
}
